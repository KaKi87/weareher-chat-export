import { exec } from 'node:child_process';
import { readFile, writeFile } from 'node:fs/promises';
import { existsSync } from 'node:fs';

import { JSDOM } from 'jsdom';

const
    AUTHOR_SELF = '',
    AUTHOR_OTHER = '',
    getMessages = async () => {
        // https://stackoverflow.com/questions/26586685/is-there-a-way-to-get-current-activitys-layout-and-views-via-adb/39923793#39923793
        await new Promise(resolve => exec(`bash -c "adb pull $(adb shell uiautomator dump | grep -oP '[^ ]+.xml') /tmp/view.xml"`, resolve));
        const { document } = (new JSDOM(await readFile('/tmp/view.xml', 'utf8'), { contentType: 'text/xml' })).window;
        return [...document.querySelectorAll('[resource-id="com.weareher.her:id/chatMessageTextView"]')].map(_ => ({
            author : _.parentElement.querySelector('[resource-id="com.weareher.her:id/chatMessageAvatarImageView"]')
                ? AUTHOR_OTHER || document.querySelector('[resource-id="com.weareher.her:id/chatHeaderTitle"]').getAttribute('text')
                : AUTHOR_SELF,
            content: _.getAttribute('text').trim()
        }));
    },
    messages = existsSync('./data.json') ? (await import('./data.json', { assert: { type: 'json' } })).default : [];

(async function _(){
    const lastMessage = messages.slice(-1)[0];
    let
        canSave = !lastMessage,
        count = 0;
    for(const message of await getMessages()){
        if(message.author === lastMessage?.author && message.content === lastMessage?.content)
            canSave = true;
        else if(canSave){
            messages.push(message);
            console.log(`[${message.author} | ${message.content}]`);
            count++;
        }
    }
    await writeFile('./data.json', JSON.stringify(messages, null, 4));
    console.log(Date.now(), count);
    await _();
})();